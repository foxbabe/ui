# UI资源 #

## 学习教程 ##
* [从零基础如何自学UI](http://www.zhihu.com/question/20857524) 
* [学UI网](http://www.xueui.cn/) UI设计师学习教育平台
* [UI设计小课堂](http://i.ui.cn/ucenter/88618.html/) 有几位牛牛在大公司工作,同时也有一颗乐于分享的心~于是他们组建了一个团队,给我们这些新手上了几堂非常宝贵的课!!非常感谢他们!!!!网页提供了很多不错的教程,而且也是免费观看的!
* [Illustrator CS6视频教程](http://www.51zxw.net/list.aspx?cid=403 )
* [photoshop百度云视频教程](http://pan.baidu.com/s/1kTDoOvX)

## 设计规范 ##
* [android设计规范-apkbus](http://www.apkbus.com/design/get-started/principles.html)
* [android设计规范-图翼网](http://www.tuyiyi.com/v/37692.html)
* [iOS7人机界面准则](http://zhuanlan.zhihu.com/langqixu/19675089)

## 设计素材库 ##
* [素材集市](http://www.sucaijishi.com/) 各种设计图标、资源参考
* [iconpng](http://www.iconpng.com/) 免费中文图标搜索引擎
* [findicon](http://findicons.com/) 中文网站，需要输入英文搜索图标
* [iconfinder](https://www.iconfinder.com/) 图标搜索，英文
* [懒人图库](http://www.lanrentuku.com/) 
* [酷站素材](http://www.zcool.com.cn/gfxs/)


## 常见设计网站 ##
* [UI中国-专业界面设计平台](http://www.ui.cn/) 国内最大的UI作品展示平台
* [UI4App](http://ui4app.com/) 国内手机App设计，按照功能、模块和控件类分组浏览
* [酷站(ZCOOL)](http://www.zcool.com.cn/) 国内设计师的主要聚集地,从平面到三维,从动画到原画,五花八门.但从首页的推荐来看,UI设计渐渐的也多了很多.同上,有很多不错的教程和设计作品,可以自己搜寻.
* [dribbble](https://dribbble.com/) 国外最有名的设计网站，追波~全球大牛聚集地，进去看看就有种要哭的赶脚
* [UI Week](http://uiweek.com/) 国内的UI杂志下载.完全免费.质量也非常不错
* [优设网](http://www.uisdc.com/) 优设网定期会邀请不同的设计师讲解一些设计原理,技巧等等.这些都是在你课堂里面学不到的喔.有很多非常实用!强烈推荐!大家可以根据自己的弱处补补.
* [Inspired-ui](http://inspired-ui.com/tagged/navigation) 设计界面参考
* [图翼网](http://www.tuyiyi.com/)设计资源
* [设计达人](http://www.shejidaren.com/) 优秀设计资源参考

## 设计工具参考 ##
* [iOS字体预览](http://iosfonts.com/)左边输入文字，右边就可以立马预览效果了~！做IOS的ui设计师必备！！
* [45个设计师需要熟记的PS快捷键](http://www.xueui.cn/tutorials/photoshop-tutorials/45-photoshop-shortcuts.html)
* [Markrman](http://www.getmarkman.com) 设计稿标注、测量神器！
* [Cutterman](http://www.cutterman.cn) PS最好的切图插件

## 设计师网址导航 ##
* [SDC设计师网址导航](http://hao.uisdc.com/)资源下载、设计教程、配色方案、界面设计、网站模板、字体设计.....等等
* [UESOSO设计师网址导航](http://so.uehtml.com/) 设计素材、设计教程、设计社区、设计公司
* [图翼设计导航](http://www.tuyiyi.com/hao/) 灵感、移动、界面、UED、图库、字体、配色、插画....等等